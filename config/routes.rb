Rails.application.routes.draw do
  resources :users

  post 'auth/signup', to: 'users#signup'
  post 'auth/signin', to: 'users#signin'
  get 'auth/test', to: 'users#test'
  get 'testRoute', to: 'users#test1'

  scope :myscope do
    get 'testScope', to: 'users#test2'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
