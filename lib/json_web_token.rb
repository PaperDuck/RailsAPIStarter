class JsonWebToken
    class << self
        def encode(payload, exp = 1.hours.from_now)
            # Set Token Expiry
            payload[:exp] = exp.to_i

            # Encodes the payload (user data) with a secrey key
            JWT.encode(payload, Rails.application.credentials.secret_key_base)
        end

        def decode(token)
            raise ExceptionHandler::MissingToken, "Missing Token" if token.nil?

            # If token is present, proceed to decode it
            body = JWT.decode(token, Rails.application.credentials.secret_key_base)[0]
            HashWithIndifferentAccess.new body

            # Raise custom error to be handled by custom handler
            rescue JWT::ExpiredSignature, JWT::VerificationError => e
                raise ExceptionHandler::ExpiredSignature, e.message
            rescue JWT::DecodeError, JWT::VerificationError => e
                raise ExceptionHandler::DecodeError, e.message
        end
    end
end