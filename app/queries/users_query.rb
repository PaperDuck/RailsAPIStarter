class UsersQuery
    attr_accessor :initial_scope

    def initialize(initial_scope = User.all)
        @initial_scope = initial_scope
    end

    def call
        return @initial_scope
    end

    def filter_by_id
        @initial_scope = @initial_scope.where(_id: id)
        return self
    end

    def filter_by_email(email)
        @initial_scope = @initial_scope.where(email: email.downcase)
        return self
    end
end