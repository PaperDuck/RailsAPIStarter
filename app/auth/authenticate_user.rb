class AuthenticateUser
    prepend SimpleCommand
    attr_accessor :email, :password

    def initialize(email, password)
        @email = email
        @password = password
    end

    def call
        JsonWebToken.encode(user_id: user.id) if user
    end

    private

    def user
        query = UsersQuery.new
        query.filter_by_email(@email) if @email
        user = query.call().first
        return user if user&.authenticate(password)

        errors.add :user_authentication, 'Invalid Credentials'
        nil
    end
end