class UsersController < ApplicationController
  # before_action :set_user, only: [:show, :update, :destroy]
  before_action :authenticate_request, only: [:test]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  def signup
    user = NewSignupService.call(user_params).result
    if user
      response = { message: "User created successfully" }
      render json: response, status: :created
    else
      raise ExceptionHandler::UserSignupError.new
    end

    return user
  end

  def signin
    authenticate params[:email], params[:password]
  end

  def test
    render json: {
      message: 'You have passed authentication and authorization test!'
    }
  end

  # Custom Route
  def test1
    render json: { msg: "Hi there! :D" }, status: :ok
  end

  # Custom Route
  def test2
    render json: { msg: "Hi there! :)"}, status: :ok
  end

  def authenticate(email, password)
    # puts "wa lauuuuuuuu"
    command = AuthenticateUser.call(email, password)

    if command.success?
      render json: {
        access_token: command.result,
        message: "Login Successful"
      }
    else
      render json: { message: "Invalid Credentials!", error: "Login Failed!" }, status: :unauthorized
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.permit(
        :email,
        :password
      )
    end
end
