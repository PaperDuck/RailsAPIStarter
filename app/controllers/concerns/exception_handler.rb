module ExceptionHandler extend ActiveSupport::Concern
    # Define custom error subclasses - rescue catches 'StandardErrors'

    class AuthenticationError < StandardError; end
    class MissingToken < StandardError; end
    class InvalidToken < StandardError; end
    class ExpiredSignature < StandardError; end
    class DecodeError < StandardError; end

    # User Errors
    class UserSignupError < StandardError; end

    included do
        # define custom handlers
        rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
        rescue_from ExceptionHandler::MissingToken, with: :unauthorized_request
        rescue_from ExceptionHandler::InvalidToken, with: :unauthorized_request
        rescue_from ExceptionHandler::ExpiredSignature, with: :unauthorized_request
        rescue_from ExceptionHandler::DecodeError, with: :unauthorized_request

        rescue_from Mongoid::Errors::DocumentNotFound do |e|
            render json: { message: e.message }, status: :not_found
        end

        rescue_from Mongoid::Errors::Validations do |e|
            render json: { message: e.message }, status: :unprocessable_entity
        end

        # User Transaction Errors
        rescue_from ExceptionHandler::UserSignupError do |e|
            render json: { message: "Unable to create user!",
                           error: e.message}, status: :bad_request
        end
    end

    private

    # JSON response with message; Status code 401 - Unauthorized
    def unauthorized_request(e)
      render json: { message: e.message }, status: :unauthorized
    end
end