class NewSignupService
    prepend SimpleCommand
    attr_accessor :params

    def initialize(params)
        @params = params
    end

    def call
        user = User.new(@params)
        if user.save
            puts 'yay'
            return user
        end
    end
end