class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  before_save { self.email = email.downcase unless email.nil? }

  field :email, type: String
  field :password_digest, type: String

  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  validates :email, uniqueness: true
end
