FROM ruby:2.4.1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

# The default command that gets ran will be to start the Puma server.
CMD rails s