## Table of Contents

---

- [General](#general)
- [Environment Setup](#environment-setup)
- [Directory Breakdown](#directory-breakdown)
    - [Services](#services)
    - [Queries](#queries)
- [Configuration](#configuration)
    - [Token Expiry](#token_expiry)
    - [Secrets](#secrets)
- [Testing](#testing)
- [Deployment Instructions](#deployment-instructions)
- [Useful Docker Commands](#useful-docker-commands)

### General
This is a boilerplate project for Rails. It comes configured with the following:
* To deploy using Docker. Config files for development and production have been created to allow for easier editing.
* Reverse Proxy for Load Balancing using Nginx.
* To work with MongoDB using Mongoid.
* JWT authentication.
* Passwords are hashed using the **bcrypt** gem.

This is just for a quick setup. Alternatively, some of the configurations can be referenced when setting up your own project.

### Environment Setup
#### Method 1: Manual

1. Make sure you have the **minimum** *Ruby Version* running on your computer. I recommend installing *Ruby* through *RVM* since it allows switching of versions easily.
2. Open a *Terminal* and install *Rails* with the following command:
   > `sudo gem install rails`

3. We now need to install the dependencies of the project on your computer. 
Change the *Terminal* to point at the project directory and then type the following command:
   > `bundle install`

4. The database files needs to be stored somewhere. So we need to create an empty folder with a structure like this.
   > `data/db`
   
       For convenience sake, open up the *Terminal* (Command Prompt of MacOS) and type in the following:
   > `mkdir -p /data/db`

This will create the data folder in the root directory. And we are done with setting up our MongoDB!

5. (Optional) To create indexes for your tables after adding them in the model file. Run the following command in the *Terminal*

    > `rake db:mongoid:create_indexes `

If you want to add in a new gem, you need to install it in your computer by doing

> `bundle`

#### Method 2: Docker
1. Check out the project from Gitlab if you haven't.
2. Install Docker CE (Community Edition) at https://store.docker.com/search?type=edition&offering=community

### Directory Breakdown
#### Services
This is a design pattern to solve the problem of **bloated** *Controllers* and *Models*. By shifting business logic code into individual classes/modules. This also helps enforce the DRY principle in software development. All Service Objects are to be created in the *app/services* folder. We are also using the *SimpleCommand* gem to simplify the usage of our Service Objects. Make sure to add *"prepend SimpleCommand"* to your Service Object file.

The naming of the Service Object should follow the convention below:
``` ruby
class NewSignupService # 'Service' Suffix
```

Calling a Service Object should look something like this:
``` ruby
command = NewSignupService.call(params) # arguements in call() are optional depending on your needs
```

You can check whether the Service succeeded with
``` ruby
command.success # This returns a boolean 
``` 

If the service returns something, you can retrieve it by
``` ruby
command.result # Retrieve the returned object through result
```

#### Queries
Query Objects are similar to Service Objects but with a more specific goal. That is to consolidate 
the query logic into a separate file from the *Controllers*. In short, we create a **single** Query Object for each *Model*. The naming of the Query Object should follow the convention below:
``` ruby
class UsersQuery # <Model in Plural Form> and 'Query' Suffix
```

Calling a Query Object should look something like this:
``` ruby
UsersQuery.new.call() # Returns a query for all Users
UsersQuery.new.filter_by_email(email).call() # Returns a query for User(s) with matching email
```

Note that all we have done is gotten a Query from the Query Object by using the call() method. To **fire** the Query, we can do this
``` ruby
myQuery1 = UsersQuery.new.call() # Return a query for all Users
myQuery1.to_a # Fires the query and get all users in an array

myQuery2 = UsersQuery.new.filter_by_email(email).call() # Returns a query for User(s) with matching email
myQuery2.first # Fires the query and get the first object
```

``` ruby
# Some methods to fire a query

myQuery1.to_a # Returns an array from the Query
myQuery1.first # Returns the first object from the Query
myQuery1.last # Returns the last object from the Query
myQuery1.count # Returns the number of objects from the Query 
```

You might realize that unlike the Service Object, we have to instantiate the Query Object by calling *new*, this is because we are not using the ***SimpleCommand*** gem, as we want to delay the call() method until we are done with chaining the filters.

### Configuration
#### Token Expiry
Right now, the JWT generated will only be valid for an hour. To modify the length of it's validity.

Go to *lib/json_web_token.rb*

And edit the default value of the exp parameter of the *encode* function
```ruby
def encode(payload, exp = 1.hours.from_now)
```

#### Secrets
Since the master.key of this project is not meant to be committed. A new master.key will have to be created after checking out this project.

By running the follow command:
    
> `bin/rails credentials:edit`

it will allow you to edit the credentials, while generating a master.key if it does not already exist.

### Testing
For testing the routes, you can use *Postman*.
You can download it at https://www.getpostman.com/ or install it as a plugin on the Chrome browser.

A collection and environment file is available in the project folder. Import them into *Postman* for testing.

### Deployment Instructions
#### Method 1: Manual
##### Development
> `rails s`
##### Production
> `rails s -e production`

#### Method 2: Docker
##### Development
> `docker-compose up`
##### Production
> `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

To **stop** the project, press *Ctrl+C* on the keyboard.

### Useful Docker Commands
***NOTE*** * An instance of a **Docker Image** is called a **Container**.

* [`docker ps`]() - Show running containers.
* [`docker-compose images`]() - List images.
* [`docker-compose build`]() - Build or rebuild services.
* [`docker-compose up`]() - Create and start containers.
* [`docker-compose down`]() - Stop and remove containers, networks, images, and volumes.
* [`docker-compose kill`]() - Kill containers.
* [`docker-compose version`]() - Show the Docker-Compose version information.